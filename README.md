# Pruebas de SmartGit
Curso de Git en YouTube para las prácticas: 
https://www.youtube.com/watch?v=4EcvyCOliUk&amp;list=PLIqQAkiA3a7oQdZPyOMRRLkDBtIlm0qlj&amp;ab_channel=AngelRamirez

Tabla de referencia cogida de https://stackedit.io/

|                |ASCII                          |HTML                         |
|----------------|-------------------------------|-----------------------------|
|Single backticks|`'Isn't this fun?'`            |'Isn't this fun?'            |
|Quotes          |`"Isn't this fun?"`            |"Isn't this fun?"            |
|Dashes          |`-- is en-dash, --- is em-dash`|-- is en-dash, --- is em-dash|